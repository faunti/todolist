import React, { Component } from 'react';
import './App.css';
import Navigation from './Components/Navigation'
import Main from './Components/Main'
import Login from './Components/Login'


class App extends Component {

    render() {

        let login = new Login();

        if(null == login.token) {
            return (<Login from={this.props.location} />);
        }
        return (
            <div>
                <Navigation/>
                <Main/>
            </div>
    );
  }
}

export default App;
