import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Login from './Login'
import Main from './Main'

class Navigation extends Component {

    constructor(props) {
        super(props);
        this.login = new Login();
        this.logout = this.logout.bind(this);
    }


    logout(e){
        e.preventDefault();
        this.login.logout();
    }

    render() {
        return (
            <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top" id="main-navigation">
                <div className="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul className="navbar-nav mr-auto">
                            <li className="nav-item"> <Link className="nav-link" to='/lists'>Lists</Link> </li>
                            <li className="nav-item"> <a href="/" className="nav-link" onClick={this.logout}>Logout</a> </li>

                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navigation