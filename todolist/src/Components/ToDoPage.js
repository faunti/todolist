import React, { Component } from 'react';
import axios from 'axios';
import Configuration from './Configuration'
import Login from './Login'
import { Link } from 'react-router-dom';

class ToDoPage extends Component {
    constructor(props) {
        super(props);
        this.login = new Login();
        this.state = {toDoLists: [], pagination: []};
        this.archive = this.archive.bind(this);
        this.sendForDelete = this.sendForDelete.bind(this);
        this.deleteList = this.deleteList.bind(this);
    }


    componentDidMount() {
        //get lists
        axios.get(Configuration.apiUrl+`lists?token=`+this.login.token)
            .then(res => {
                this.setState({
                        toDoLists: res.data.data,
                        pagination: res.data
                    });
            });

        //check if user is admin
        axios.get(Configuration.apiUrl+'users/isAdmin?token='+this.login.token)
            .then(res => {
                console.log(res.data);
                this.setState({isAdmin: res.data.isAdmin});
            });

    }

    deleteList(e,toDoList){
        //completele delete - only administrator can do it
        axios.delete(Configuration.apiUrl+`lists/`+toDoList+`?token=`+this.login.token)
            .then(res => {
                console.log(res);
            });
    }

    sendForDelete(e,toDoList){
        //set status to delete - kind of a soft delete
        axios.put(Configuration.apiUrl+`lists/delete/`+toDoList+`?token=`+this.login.token,
            {
                status: 'delete'
            })
            .then(res => {
                console.log(res);
            });
    }

    archive(e,toDoList){
        //set status to archived
        axios.put(Configuration.apiUrl+`lists/archive/`+toDoList+`?token=`+this.login.token,
            {
                status: 'archived'
            })
            .then(res => {
                console.log(res);
            });
    }


    render() {
        return(
            <div>
                <h3>Your To Do Lists</h3>
                <ul className="list-group">
                    {this.state.toDoLists.map((toDoList) =>
                            <li className="list-group-item" key={toDoList.id}>
                                <Link to={`/list/${toDoList.id}`}>{toDoList.name}</Link>
                                {this.state.isAdmin?
                                    (<button className="btn btn-primary pull-right" onClick={e => this.deleteList(e,toDoList.id)}>Delete</button>)
                                    :(
                                        <button className="btn btn-primary pull-right" onClick={e => this.sendForDelete(e,toDoList.id)}>Send For Deletion</button>
                                    )}
                                <button className="btn btn-primary pull-right" onClick={e => this.archive(e,toDoList.id)}>Archive</button>
                                </li>
                        )}
                </ul>
                <Link to={'/list/new'} role="button" className="btn btn-primary" onclick={this.preventSubmit}>New</Link>
            </div>
        );
    }

    preventSubmit(e){
        e.preventDefault();
        e.preventBubble();
    }




}

export default ToDoPage;