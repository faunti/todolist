import React, { Component } from 'react';

class ToDoList extends Component {
    render() {
        return (
            <ul className="list-group">
                {this.props.items.map(item => (
                    <li className="list-group-item" key={item.id}>{item.body}
                    </li>
                ))}
            </ul>
        );
    }
}

export default ToDoList
