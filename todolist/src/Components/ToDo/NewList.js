import React, { Component } from 'react';
import ToDoList from '../ToDoList';
import axios from 'axios';
import Configuration from '../Configuration'
import Login from '../Login'

class NewList extends Component {
    constructor(props) {
        super(props);
        this.state = { items: [], text: '', name: ''};
        this.login = new Login();

        this.handleChange = this.handleChange.bind(this);
        this.saveName = this.saveName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.submitList = this.submitList.bind(this);
    }

    render() {
        return (
            <div>
                {this.state.showCreateSuccess ?(
                    <div className="alert alert-success">
                        <strong>Success!</strong> User created.
                    </div>
                ):''}
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group row">
                        <label className="col-md-4 col-form-label">Name:</label>
                        <input required={true} type="text" value={this.state.name} onChange={this.saveName} name={'name'} className="form-control"/>
                    </div>

                    <ToDoList items={this.state.items} />

                    <input
                        onChange={this.handleChange}
                        value={this.state.text}
                    />
                    <button className="btn btn-primary">
                        Add #{this.state.items.length + 1}
                    </button>
                </form>
                <button className="btn btn-primary" onClick={this.submitList}>
                    Save
                </button>
            </div>
        );
    }

    submitList(e){
        e.preventDefault();
        //create new list with all its tasks at once
        axios.post(Configuration.apiUrl+`lists?token=`+this.login.token, {
                items: this.state.items,
                name: this.state.name
            })
            .then(
                res => {
                    this.setState({'showCreateSuccess':true})
                    console.log(this.state.showCreateSuccess);
                },
                error => { console.log(error) }
            );
    }

    saveName(e){
        this.setState({ name: e.target.value });
    }

    handleChange(e) {
        this.setState({ text: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        const newItem = {
            body: this.state.text,
            id: Date.now()
        };
        this.setState(prevState => ({
            items: prevState.items.concat(newItem),
            text: ''
        }));
    }


}

export default NewList;