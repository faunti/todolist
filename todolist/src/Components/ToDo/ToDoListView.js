import React, { Component } from 'react';
import axios from 'axios';
import Configuration from '../Configuration'
import Login from '../Login'
import ToDoList from '../ToDoList'

class ToDoListView extends Component {

    constructor(props){
        super(props);
        this.login = new Login();
        this.state = {items: [], text: '', name: ''};
        this.deleteTask = this.deleteTask.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        console.log(this.props);

        //get lists
        axios.get(Configuration.apiUrl+'lists/'+this.props.match.params.id+'?token='+this.login.token)
            .then(res => {
                console.log(res.data);
                this.setState({
                    name: res.data.name,
                    items: res.data.tasks
                });
            });
    }

    deleteTask(e,id){
        e.preventDefault();
        //delete from app state
        function findById(item) {
            return item.id == id;
        }

        this.state.items.splice(this.state.items.findIndex(findById),1);
        this.setState(this.state);

        //delete from abck end
        axios.delete(Configuration.apiUrl+'tasks/'+id+'?token='+this.login.token)
            .then(res => {
                this.setState({
                    deleteSuccess: true
                });
            },
                error =>{
                console.log(error);
                });

    }

    markDone(e,id){
        console.log(e.target.checked);

        //mark in app state
        function findById(item) {
            return item.id == id;
        }

        this.state.items[this.state.items.findIndex(findById)].status=e.target.checked;
        this.setState(this.state);
        //persist to backend
        axios.put(Configuration.apiUrl+'tasks/'+id+'?token='+this.login.token,
            {
                completed:e.target.checked
            }
            )
            .then(res => {
                    this.setState({
                        showCreateSuccess: true
                    });
                },
                error =>{
                    console.log(error);
                });

    }

    render() {

        return(
            <div>
                {this.state.showCreateSuccess ?(
                    <div className="alert alert-success">
                        <strong>Success!</strong> List Updated.
                    </div>
                ):''}
                {this.state.deleteSuccess ?(
                    <div className="alert alert-success">
                        <strong>Success!</strong> Task Deleted.
                    </div>
                ):''}
                <h1>{this.state.name}</h1>
                <form onSubmit={this.handleSubmit}>

                    <ul className="list-group">
                        {this.state.items.map(item => (
                            <li className="list-group-item" key={item.id}>
                                <input type={'checkbox'} onChange={e => this.markDone(e,item.id)} checked={item.status}/>
                                {item.body} - created at: {item.created_at?( item.created_at):'now'}
                                <button className="btn btn-primary pull-right" onClick={e => this.deleteTask(e,item.id)}>Delete</button>
                            </li>
                        ))}
                    </ul>

                    <input
                        onChange={this.handleChange}
                        value={this.state.text}
                    />
                    <button className="btn btn-primary">
                        Add #{this.state.items.length + 1}
                    </button>
                </form>

                <button className="btn btn-primary">
                    Export as Excel
                </button>
            </div>
        );
    }

    handleChange(e) {
        this.setState({ text: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        //do not allow empty text
        if (!this.state.text.length) {
            return;
        }
        //add new task to app state
        const newItem = {
            body: this.state.text,
            id: Date.now()
        };
        this.setState(prevState => ({
            items: prevState.items.concat(newItem),
            text: ''
        }));

        //add new task to list BE
        axios.put(Configuration.apiUrl+'lists/'+this.props.match.params.id+'?token='+this.login.token,
            {
                task: this.state.text
            })
            .then(res => {
                console.log('added task');
            });

    }
}

export default ToDoListView