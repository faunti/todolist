import React, { Component } from 'react';
import {
    Redirect
} from 'react-router-dom'



import Navigation from "./Navigation";
import Configuration from './Configuration'
import axios from 'axios';
import Cookies from 'universal-cookie'


class Login extends Component {
    token = null;

    constructor(props) {
        super(props);

        this.cookies = new Cookies();
        this.token = this.cookies.get('session');

        this.state = {
            redirectToReferrer: false,
            register: {},
            login: {},
            showCreateSuccess: false,
        };

        this.register = this.register.bind(this);
        this.handleChangeRegister = this.handleChangeRegister.bind(this);

        this.login = this.login.bind(this);
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
    }



    logout(){
        //remove cookie and go to home page
        document.cookie = 'session'+'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        window.location = '/';
    }

    login(e){
        e.preventDefault();

        //check with server and save cookie if successful
        axios.post(Configuration.apiUrl+`users/login`,
            this.state.login
        )
            .then(
                res => {
                    this.cookies.set('session', res.data.remember_token, { path: '/' });
                    console.log(this.cookies.get('session'));

                    this.setState({ token: res.data.remember_token })
                    this.setState({ redirectToReferrer: true })
                    console.log(this.state.showCreateSuccess);
                },
                error => { console.log(error) }
            );

    }

    register(e){
        e.preventDefault();
        axios.post(Configuration.apiUrl+`users`,
            this.state.register
            )
            .then(
                res => {
                this.setState({'showCreateSuccess':true})
                console.log(this.state.showCreateSuccess);
                },
                error => { console.log(error) }
            );
    }

    handleChangeRegister(e){
        this.state.register[e.target.name] = e.target.value;
    }

    handleChangeLogin(e){
        this.state.login[e.target.name] = e.target.value;
    }

    render() {
        const { from } = this.props.from || { from: { pathname: '/lists' } }
        const { redirectToReferrer } = this.state

        if (redirectToReferrer) {
            return (
                <Redirect to={from}/>
            )
        }

        return (
            <div>
                <Navigation />
                <div className="main-container">
                    <h1> Authentication </h1>
                    <p>You must log in to view the page at {from.pathname}</p>
                    <p>Please login or register.</p>
                    <div className={'row'}>
                        <div className={'col-sm-6'}>
                            <form onSubmit={this.login}>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label">Email:</label>
                                    <input type="email" onChange={this.handleChangeLogin} name={'email'} className="form-control col-md-8" />
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label">Password:</label>
                                    <input type="password" onChange={this.handleChangeLogin} name={'password'} className="form-control col-md-8"/>
                                </div>
                                    <button type="submit" className="btn btn-primary">Log in</button>
                            </form>
                        </div>
                        <div className={'col-sm-6'}>

                            {this.state.showCreateSuccess ?(
                            <div className="alert alert-success">
                                <strong>Success!</strong> User created.
                            </div>
                            ):''}

                            <form onSubmit={this.register}>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label">Name:</label>
                                    <input type="text" value={this.state.register.name} onChange={this.handleChangeRegister} name={'name'} className="form-control col-md-8" />
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label">Email:</label>
                                    <input type="email" value={this.state.register.email} onChange={this.handleChangeRegister} name={'email'} className="form-control col-md-8" />
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label">Password:</label>
                                    <input type="password" value={this.state.register.password} onChange={this.handleChangeRegister} name={'password'} className="form-control col-md-8"/>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label">Repeat Passwor:</label>
                                    <input type="password" value={this.state.register.password_confirmation} onChange={this.handleChangeRegister} name={'password_confirmation'} className="form-control col-md-8"/>
                                </div>
                                <button type="submit" className="btn btn-primary">Register </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login