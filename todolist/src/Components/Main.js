import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import ToDoPage from './ToDoPage'
import ToDoListView from './ToDo/ToDoListView';
import NewList from './ToDo/NewList'

class Main extends Component {

    //render router
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path='/lists' component={ToDoPage}/>
                    <Route path='/list/new' component={NewList}/>
                    <Route path='/list/:id' component={ToDoListView} />
                </Switch>
            </div>
        );
    }
}

export default Main
