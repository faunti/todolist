<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$restMethods = ['index', 'store', 'show', 'update', 'destroy'];

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::put('lists/archive/{toDoList}', ToDoListController::class.'@archive');
Route::put('lists/delete/{toDoList}', ToDoListController::class.'@deleteRequest');
Route::get('lists/export/{toDoList}', ToDoListController::class.'@exportExcel');
Route::get('lists/{toDoList}', ToDoListController::class.'@show');
Route::put('lists/{toDoList}', ToDoListController::class.'@update');
Route::delete('lists/{toDoList}', ToDoListController::class.'@destroy');
Route::resource('lists', ToDoListController::class, ['only'=>$restMethods]);

Route::resource('tasks', TaskController::class, ['only'=>$restMethods]);

Route::get('users/isAdmin',UserController::class.'@isAdmin');
Route::resource('users', UserController::class, ['only'=>$restMethods]);
Route::post('users/login',UserController::class.'@login');

