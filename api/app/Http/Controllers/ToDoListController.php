<?php

namespace App\Http\Controllers;

use App\Task;
use App\ToDoList;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ToDoListController extends Controller
{

    public function index(ToDoList $toDoList,  Request $request){
        $token = $request->get('token');
        $user = User::where('remember_token',$token)->first();

        if(!$user){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        $query = $toDoList;
        if(!$user->is_admin){
            $query = $toDoList->where('creator_id',$user->id)->where('status','new');
        }


        return response()->json($query->paginate());
    }

    public function show(ToDoList $toDoList,  Request $request){
        if($this->checkToken($request)){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }
        return response()->json($toDoList);
    }

    public function store(ToDoList $toDoList, Request $request){

        $token = $request->get('token');
        $user = User::where('remember_token',$token)->first();

        if(!$user){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        DB::beginTransaction();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validator->fails()){
            DB::rollBack();
            return response()->json(['errors' => $validator->messages()],422);
        }

        $toDoList->name=$request->get('name');
        $toDoList->creator_id = $user->id;
        $toDoList->status = 'new';
        $toDoList->save();

        foreach ($request->get('items') as $item){
            $validator = Validator::make($item, [
                'body' => 'required',
            ]);
            if($validator->fails()){
                DB::rollBack();
                return response()->json(['errors' => $validator->messages()],422);
            }

            $task = new Task();
            $task->status=0;
            $task->body = $item['body'];
            $task->to_do_list_id = $toDoList->id;
            $task->save();
        }

        DB::commit();

        return response()->json(['success'=>true]);

    }

    public function update(ToDoList $toDoList, Request $request){

        if($this->checkToken($request)){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        $task = new Task();
        $task->status = 0;
        $task->body = $request->get('task');
        $task->to_do_list_id = $toDoList->id;
        $task->save();

        return response()->json(['success'=>true]);

    }

    protected function checkToken(Request $request){
        $token = $request->get('token');
        $user = User::where('remember_token',$token)->first();
        return !$user;
    }

    public function exportExcel(ToDoList $toDoList, Request $request){

        $excel = App::make('excel');

        $tasks = Task::where('to_do_list_id', $toDoList->id)->get();

        $excel->create($toDoList->name, function($excel) use ($tasks) {

            $excel->sheet('Sheet 1', function($sheet) use ($tasks) {

                $sheet->fromModel($tasks);

            });

        })->export('xls');

    }

    public function destroy(ToDoList $toDoList, Request $request){
        $token = $request->get('token');
        $user = User::where('remember_token',$token)->first();

        if(!$user){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        if(!$user->is_admin){
            return response()->json(['errors' => ['token'=>'You are not authorised!']]);
        }

        $toDoList->delete();

    }

    public function deleteRequest(ToDoList $toDoList, Request $request){
        if($this->checkToken($request)){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        $toDoList->status = 'delete';
        $toDoList->save();

        return response()->json(['success'=>true]);
    }

    public function archive(ToDoList $toDoList, Request $request){
        if($this->checkToken($request)){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        if($toDoList->tasks()->count()<1){
            return response()->json(['errors' => ['token'=>'Cannot archive if only one task is present!']]);
        }

        $toDoList->status = 'archive';
        $toDoList->save();

        return response()->json(['success'=>true]);
    }

}

