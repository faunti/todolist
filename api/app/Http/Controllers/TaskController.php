<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function destroy(Task $task, Request $request){
        if($this->checkToken($request)){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        $task->delete();
        return response()->json(['success'=>true]);
    }

    public function update(Task $task, Request $request){
        if($this->checkToken($request)){
            return response()->json(['errors' => ['token'=>'You are not authenticated!']]);
        }

        $task->status = $request->get('completed');
        $task->save();

        return response()->json(['success'=>true]);
    }

    protected function checkToken(Request $request){
        $token = $request->get('token');
        $user = User::where('remember_token',$token)->first();
        return !$user;
    }
}
