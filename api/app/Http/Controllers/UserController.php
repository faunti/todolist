<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function store(User $user, Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|confirmed',
            'password_confirmation'
        ]);

        if($validator->fails()){
            return response()->json(['errors' => $validator->messages()],422);
        }

        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->password = Input::get('password');
        $user->is_admin = 0;
        $user->save();

        return response()->json(['success'=>true]);
    }

    public function login(){

        $user = User::where('email',Input::get('email'))
            ->where('password',Input::get('password'))
            ->first();

        if($user != null){
            $user->remember_token = md5(Input::get('email') . time() . rand(100000,999999));
            $user->save();
        }


        return response()->json([
            'remember_token'=> $user->remember_token
        ]);
    }

    public function isAdmin(Request $request){
        $token = $request->get('token');
        return response()->json(['isAdmin'=>User::where('remember_token',$token)->first()->is_admin]);
    }
}
