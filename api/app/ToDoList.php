<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDoList extends Model
{
    protected $table = 'lists';
    protected $fillable = ['id','name'];
    protected $with = ['tasks'];

    public function tasks(){
        return $this->hasMany(Task::class);
    }
}
